<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materiel extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom_mat',
        'type',
        'qte',
        'marque',
        'etat',
        'caracteristique'
    ];

    public function technicien()
    {
        return $this->belongsTo(Technicien::class, 'id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function marque()
    {
        return $this->belongsTo(Marque::class, 'id');
    }
}
