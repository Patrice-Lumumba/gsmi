<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Technicien extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
//    'num_service',
        'name',
        'prenom',
        'adresse',
        'tel',
        'email'
    ];

    public function materiel()
    {
        return $this->belongsTo(Materiel::class, 'id');
    }
}
