<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'action',
        'details'
    ];
}

Log::create([
//    'id' => auth()->id(),
    'action' => 'Action effectuée',
    'details' => 'Description détaillée de l\'action',
]);
