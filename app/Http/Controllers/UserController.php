<?php

    namespace App\Http\Controllers;

    use App\Models\User;
    use Illuminate\Contracts\Foundation\Application;
    use Illuminate\Contracts\View\Factory;
    use Illuminate\Contracts\View\View;
    use Illuminate\Http\RedirectResponse;
    use Illuminate\Http\Request;

    class UserController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return Application|Factory|View
         */
        public function index()
        {
            $user = User::paginate(5);
            $user = User::orderBy('created_at', 'DESC')->get();

            return view('users.index', compact('user'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return Application|Factory|View
         */
        public function create()
        {
            //
            return view('users.add');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param Request $request
         * @return RedirectResponse
         */
        public function store(Request $request)
        {
            //
            User::create($request->all());

            return redirect()->route('users')->with('success', 'User added successfully');
            $user = new User();

//            $user->tel = $request['tel'];
            $user->email =  $request['email'];
            $user->name =  $request['name'];

//             $user->save();

            if ($user) {
                return redirect()->route('users')->with('success', 'User added successfully');
            }

        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return Application|Factory|View
         */
        public function show($id)
        {
            //
            $user = User::findOrFail($id);

            return view('users.edit', compact('user'));
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return Application|Factory|View
         */
        public function edit($id)
        {
            //
            $user = User::findOrFail($id);

            return view('users.edit', compact('user'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param Request $request
         * @param  int  $id
         * @return RedirectResponse
         */
        public function update(Request $request, $id)
        {
            //
            $users = User::findOrFail($id);

            $users->update($request->all());

            return redirect()->route('users')->with('success', 'User updated successfully');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return RedirectResponse
         */
        public function destroy($id)
        {
            //
            $users = User::findOrFail($id);

            $users->delete();

            return redirect()->route('users')->with('success', 'User deleted successfully');
        }


    }
