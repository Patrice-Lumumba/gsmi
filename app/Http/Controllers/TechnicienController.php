<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Technicien;
use Illuminate\Http\Request;

class TechnicienController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $tech = Technicien::paginate(5);
        return view('technicien.index', compact('tech'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('technicien.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Technicien::create($request->all());
        return redirect()->route('technicien')->with('success', 'Technicien ajouté avec succès');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
