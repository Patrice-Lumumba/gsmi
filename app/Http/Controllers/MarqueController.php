<?php

namespace App\Http\Controllers;

use App\Models\Marque;
use Illuminate\Http\Request;

class MarqueController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $marque = Marque::paginate(5);
        return view('marque.index', compact('marque'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('marque.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Marque::create($request->all());
        return redirect()->route('marque')->with('success', 'La marque a été ajoutée avec succès');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $marque = Marque::findOrFail($id);

        $marque->delete();

        return redirect()->route('marque')->with('success', 'Marque deleted successfully');
    }
}
