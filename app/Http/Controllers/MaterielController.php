<?php

namespace App\Http\Controllers;

use App\Models\Marque;
use App\Models\Materiel;
use App\Models\Service;
use App\Models\Technicien;
use App\Models\type;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

class MaterielController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
//        $materiel = Materiel::orderBy('created_at', 'ASC')->get();
        $materiel = Materiel::paginate(5);
        return view('materiel.index', compact('materiel'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $marque = Marque::pluck('titre', 'id');
        $tech = Technicien::pluck('prenom', 'id');

        $services = Service::pluck('nom_service', 'service_id');
        return view('materiel.add', compact('marque', 'services', 'tech'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
//        $materiels = Materiel::with('service:id,nom')->get();
        Materiel::create($request->all());
        return redirect()->route('materiel')->with('success', 'Le matériel a été ajouté avec succès');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $materiel = Materiel::findOrFail($id);
        $marque = Marque::pluck('titre', 'id');
        $tech = Technicien::pluck('prenom', 'id');

        $services = Service::pluck('nom_service', 'service_id');
//        return view('materiel.add', compact('marque', 'services', 'tech'));


        return view('materiel.show', compact('marque', 'services', 'tech', 'materiel'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $materiel = Materiel::findOrFail($id);

        return view('users.edit', compact('materiel'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $materiel = Materiel::findOrFail($id);

        $materiel->update($request->all());

        return redirect()->route('materiel')->with('success', 'Materiel updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {

        $materiel = Materiel::findOrFail($id);

        $materiel->delete();

        return redirect()->route('materiel')->with('error', 'Le matériel a été supprimé');
    }

//    public function chirps(): HasMany
//    {
//        return  $this->hasMany(type::class);
//    }
}
