<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function search(Request $request){
        if($request->ajax()){

            $data = Post::where('id', 'like', '%'.$request->search.'%')
                ->orwhere('nom', 'like', '%'.$request->search.'%')
                ->orwhere('tel', 'like', '%'.$request->search.'%')
                ->orwhere('adresse', 'like', '%'.$request->search.'%')
                ->orwhere('email', 'like', '%'.$request->search.'%')->get();

            $output = '';
            if (count($data)>0){

                $output='
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Tel</th>
                            <th scope="col">Adresse</th>
                            <th scope="col">Email</th>
                        </tr>
                        </thead>
                        <tbody>';

                foreach($data as $row){
                    $output .='
                            <tr>
                                <th scope="row">'.$row->id.'</th>
                                <th>'.$row->id.'</th>
                                <td>'.$row->nom.'</td>
                                <td>'.$row->tel.'</td>
                                <td>'.$row->adresse.'</td>
                                <td>'.$row->email.'</td>
                            </tr>
                                ';
                }

                $output .='
                              </tbody>
                                 </table>';

            }


        }
        else{
            $output = 'No results';
        }
        return $output;

    }
}
