<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class LearnController extends Controller
{
    //
    public function __invoke(Request $request): void
    {
        $order = [
            [
                'id' => 1,
                'product' => 'Product 1',
                'price' => 100,
                'quantity' => 2,
            ],
            [
                'id' => 1,
                'product' => 'Product 2',
                'price' => 200,
                'quantity' => 2,
            ],
            [
                'id' => 1,
                'product' => 'Product 3',
                'price' => 300,
                'quantity' => 3,
            ],
        ];

        $total = 600;

//        $pdf = Pdf::loadView('invoice', compact('order', 'total'));
//        $pdf = Pdf::localView('invoice', compact('order', 'total'));
    }
}
