<?php

namespace App\Helpers;

use App\Models\Message;

class Helper
{
    public static function getRole()
    {
        $data = [];
        $data['admin'] = 'Admin';
        $data['user'] = 'User';

        return $data;
    }

    public static function getTypeMateriel()
    {
        $data = [];
        $data['laptop'] = 'Laptop';
        $data['desktop'] = 'Desktop';

        return $data;
    }

    public static function getEtatMateriel() {
        $data = [];
        $data['panne'] = 'En panne';
        $data['ras'] = 'Bon état';
        $data['verified'] = 'A vérifier';

        return $data;
    }

    public static function getMarqueMateriel() {
        $data = [];
        $data['hp'] = 'HP';
        $data['dell'] = 'DELL';
        $data['asus'] = 'ASUS';

        return $data;
    }







    public static function messageList()
    {
        return Message::whereNull('read_at')->orderBy('created_at', 'desc')->get();
    }

}
