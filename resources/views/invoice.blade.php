<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice Page</title>
</head>
<body>
    <h1>INVOICE</h1>
    <ul>
        @foreach($order as $data)
            <li>Product: {{$data['product']}}</li>
            <li>Price: {{$data['price']}}</li>
            <li>Quantity: {{$data['quantity']}}</li>
        @endforeach
    </ul>
    <hr>
    <h3>
        Total:{{$total}}
    </h3>
<div class="page-break"></div>
<p>
    By Patrice Lumumba
</p>
</body>
</html>
