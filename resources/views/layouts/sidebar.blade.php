<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('dashboard')}}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>



    <li class="nav-item">
        <a class="nav-link" href="{{ route('users') }}">
            <i class="fas fa-fw fa-user"></i>
            <span>Users</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('service') }}">
            <i class="fas fa-fw fa-bomb"></i>
            <span>Services</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('materiel') }}">
            <i class="fas fa-fw fa-envira"></i>
            <span>Matériel</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('technicien') }}">
            <i class="fas fa-fw fa-dashboard"></i>
            <span>Technicien</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('marque') }}">
            <i class="fas fa-fw fa-code-branch"></i>
            <span>Marque</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('logs') }}">
            <i class="fas fa-fw fa-font-awesome-logo-full"></i>
            <span>Historique</span></a>
    </li>
    <div id="categoryCollapse" class="collapse nav-item" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Options du technicien :</h6>
            <a class="collapse-item nav-link" href="{{route('technicien')}}">Techniciens</a>
            <a class="collapse-item nav-link" href="">Ajouter un technicien</a>
        </div>
    </div>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>




</ul>
