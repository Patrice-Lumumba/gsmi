@extends('layouts.master')

@section('title', 'Create Technician')

@section('contents')
    <h4 class="mb-0">Add Technicien</h4>
    <hr/>
    <form method="post" action="{{ route('technicien.store') }}">
        @csrf
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label class="col-form-label">Nom</label>
                    <input type="text" class="form-control" name="nom" placeholder="Entrer votre nom">
                    @error('nom')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="col-md-6">
                    <label class="col-form-label">Prenom</label>
                    <input type="text" class="form-control" name="prenom" placeholder="Entrer votre prénom">
                    @error('firstname')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="col-md-6">
                    <label class="col-form-label">Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Adresse email">
                    @error('email')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label class="col-form-label">Téléphone</label>
                    <input type="text" class="form-control" name="tel" placeholder="Téléphone">
                    @error('tel')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label class="col-form-label">Adresse</label>
                    <input type="text" class="form-control" name="adresse" placeholder="Téléphone">
                    @error('adresse')
                    <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
        </div>

{{--        <div class="form-group">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-6">--}}
{{--                    <label class="col-form-label">Téléphone</label>--}}
{{--                    <input type="text" class="form-control" name="tel" placeholder="Téléphone">--}}
{{--                    @error('tel')--}}
{{--                    <span class="text-danger">{{$message}}</span>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="form-group mb-3">
            <button class="btn btn-success" type="submit">Enregistrer</button>
        </div>
    </form>
@endsection
