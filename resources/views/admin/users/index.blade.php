@extends('admin.layouts.master')
<?php $title_page = 'Users list'?>

@section('main-content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="row">
            <div class="col-md-12">
                @include('admin.layouts.notification')
            </div>
        </div>
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary float-left">Users List</h6>
            <a href="{{route('users.create')}}" class="btn btn-primary btn-sm float-right" data-toggle="tooltip" data-placement="bottom" title="Add User"><i class="fas fa-plus"></i> Add User</a>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <input type="text" class="form-control" name="search" id="search" placeholder="Enter User name">
            </div>
            <div id="search_list"></div>
        </div>
        <div class="col-lg-3"></div>

        <script>
            $(document).ready(function(){
                $('#search').on('keyup',function (){
                    var query = $(this).val();
                    $.ajax({
                        url: "search",
                        type: "GET",
                        data: {'search': query},
                        success: function (data) {
                            $('#search_list').html(data);
                        }
                    });
                });
            });
        </script>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nom</th>
                <th scope="col">Tel</th>
                <th scope="col">Adresse</th>
                <th scope="col">Email</th>
            </tr>
            </thead>
            <tbody>
                foreach($data as $row){
                    $output .=
                <tr>
                    <th scope="row">.$row->id.</th>
                    <th>.$row->id.</th>
                    <td>.$row->nom.</td>
                    <td>.$row->tel.</td>
                    <td>.$row->adresse.</td>
                    <td>.$row->email.</td>
                </tr>

                }

                    $output .='
                            </tbody>
                            </table>';

        }



        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="alert-text">{{ session('success') }}</span>
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger alert-dismissible fade show " role="alert">
                    <span class="alert-text">{{ session('error') }}</span>
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-bordered" id="user-dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Photo</th>
                        <th>Join Date</th>
                        <th>Téléphone</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td><a href="{{route('users.show',$user->id)}}" style="text-decoration: none">{{$user->firstname}}</a></td>
                            <td>{{$user->email}}</td>
                            <td>
                                @if($user->photo)
                                    <img src="{{$user->photo}}" class="img-fluid rounded-circle" style="max-width:50px" alt="{{$user->photo}}">
                                @else
                                    <img src="{{asset('admin_assets/img/avatar.png')}}" class="img-fluid rounded-circle" style="max-width:50px" alt="avatar.png">
                                @endif
                            </td>
                            <td>{{(($user->created_at)? $user->created_at->diffForHumans() : '')}}</td>
                            <td>{{$user->tel}}</td>
                            <td>
                                @if($user->status=='active')
                                    <span class="badge badge-success">{{$user->status}}</span>
                                @else
                                    <span class="badge badge-warning">{{$user->status}}</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('users.show',$user->id)}}" class="btn btn-secondary btn-sm ml-3" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="edit" data-placement="bottom"><i class="fas fa-eye"></i></a>
                                <a href="{{route('users.edit',$user->id)}}" class="btn btn-primary btn-sm float-left mr-1" style="height:30px; width:30px;border-radius:50%" data-toggle="tooltip" title="edit" data-placement="bottom"><i class="fas fa-edit"></i></a>
                                <form method="POST" action="{{route('users.destroy',[$user->id])}}">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger btn-sm dltBtn" data-id={{$user->id}} style="height:30px;width:30px;border-radius:50%" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fas fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link href="{{asset('backend/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <style>
        div.dataTables_wrapper div.dataTables_paginate{
            display: none;
        }
    </style>
@endpush

@push('scripts')

    <script src="{{asset('backend/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('backend/js/demo/datatables-demo.js')}}"></script>
    <script>

        $('#user-dataTable').DataTable( {
            "columnDefs":[
                {
                    "orderable":false,
                    "targets":[6,7]
                }
            ]
        } );

        // Sweet alert

        function deleteData(id){

        }
    </script>
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.dltBtn').click(function(e){
                var form=$(this).closest('form');
                var dataID=$(this).data('id');
                // alert(dataID);
                e.preventDefault();
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this data!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        } else {
                            swal("Your data is safe!");
                        }
                    });
            })
        })
    </script>
@endpush
