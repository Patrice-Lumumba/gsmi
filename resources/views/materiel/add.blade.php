@php use App\Helpers\Helper;use App\Models\Materiel;use App\Models\Service; @endphp
@extends ('layouts.master')

@section('title', 'Matériel')
@section('contents')

    <h1 class="">Créer / Ajouter un Matériel</h1>
    <form action="{{route('materiel.store')}}" method="post">
        {{@csrf_field()}}
        <div class="form-group">
            <label for="code_mat">Titre: </label>
            <input type="text" name="nom_mat" id="nom_mat" placeholder="Ex: PC" class="form-control">
        </div>

        <div class="form-group">
            <label for="type">Type: </label>
            <select name="type" id="type" class="form-control">
                <option value="" selected>-----------Select------------</option>

                @foreach(Helper::getTypeMateriel() as $key=>$t)
                    <option value='<?=$key?>'>{{$t}}</option>
                @endforeach
            </select>
            {{--            <input type="text" name="label_mat" id="label_mat" placeholder="Ex: Mathématiques" class="form-control">--}}
        </div>

        <div class="form-group">
            <label for="type">Etat: </label>
            <select name="etat" id="etat" class="form-control">
                <option value="" selected>-----------Select------------</option>

                @foreach(Helper::getEtatMateriel() as $key=>$t)
                    <option value='<?=$key?>'>{{$t}}</option>
                @endforeach
            </select>
            {{--            <input type="text" name="label_mat" id="label_mat" placeholder="Ex: Mathématiques" class="form-control">--}}
        </div>

        <div class="form-group">
            <label for="credit_mat">Quantité </label>
            <input type="number" name="qte" id="qte" class="form-control">
        </div>

        <div class="form-group">
            <label for="titre">Marque: </label>
            <select name="marque" id="materiel" class="form-control">
                <option value="" selected>Sélectionner la marque</option>

                @foreach($marque as $key=>$t)
                    <option value='<?=$key?>'>{{$t}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="caracteristique">Caracteristique: </label>
            <textarea name="caracteristique" id="caracteristique" class="form-control"></textarea>
        </div>



        <div class="row mt-2" style="text-align: right;">
            <input type="submit" value="valider" class="btn btn-success col-2">
        </div>
    </form>
@endsection
