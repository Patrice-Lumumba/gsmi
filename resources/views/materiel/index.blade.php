@extends('layouts.master')

@section('title', 'Matériels')

@section('contents')
    <div class="container">
    <div class="d-flex align-items-center justify-content-between">
        <h1 class="mb-0">Liste des matériels</h1>
        <a href="{{ route('materiel.create') }}" class="btn btn-primary">Ajouter matériel</a>
    </div>
    <hr />
    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    <table class="table table-responsive">
        <thead class="table-primary">
        <tr>
            <th>#</th>
            <th>Nom du matériel</th>
            <th>Type</th>
            <th>Quantité</th>
            <th>Marque</th>
            <th>Etat</th>
            <th>Caractéristique</th>

            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @if($materiel->count() > 0)
            @foreach($materiel as $rs)
                <tr>
                    <td class="align-middle">{{ $loop->iteration }}</td>
                    <td class="align-middle">{{ $rs->nom_mat }}</td>
                    <td class="align-middle">{{ $rs->type }}</td>
                    <td class="align-middle">{{ $rs->qte }}</td>
                    <td class="align-middle">{{ $rs->marque }}</td>
                    @if($rs->etat == 'ras')
                        <td class="align-middle"><span class="badge badge-success">{{$rs->etat}}</span></td>
                    @elseif($rs->etat == 'panne')
                        <td class="align-middle"><span class="badge badge-danger">{{$rs->etat}}</span></td>
                    @else($rs->etat == 'verified')
                        <td class="align-middle"><span class="badge badge-warning">{{$rs->etat}}</span></td>
                    @endif

{{--                    <td class="align-middle">{{ $rs->etat }}</td>--}}
{{--                    <td class="align-middle">{{ $rs->etat }}</td>--}}
                    <td class="align-middle">{{ $rs->caracteristique }}</td>

                    <td class="align-middle">
                        <div class="btn-group" role="group" aria-label="Basic example">
{{--                            <a href="{{ route('users.show', $rs->id) }}" type="button" class="btn btn-secondary">Detail</a>--}}
                            <a href="{{ route('materiel.edit', $rs->id)}}" type="button" class="btn btn-warning">Edit</a>
                            <a href="{{ route('materiel.show', $rs->id)}}" type="button" class="btn btn-success">Maintenance</a>
                            <form action="{{ route('materiel.destroy', $rs->id) }}" method="POST" type="button" class="btn btn-danger p-0" onsubmit="return confirm('Delete?')">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger m-0" onsubmit="return confirm('Delete?')">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="8">Material not found</td>
            </tr>
        @endif
        </tbody>
    </table>
    {{$materiel->links()}}
    </div>
@endsection
