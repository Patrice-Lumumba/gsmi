@extends('layouts.master')

@section('title', 'Maintenance')

@section('contents')
    <form action="" method="post">
        <h1 class="mb-0">Affectation du matériel à un technicien</h1>
        <hr />
        <div class="row">
            <div class="col mb-3">
                <label class="form-label">Nom:</label>
                <input type="text" name="title" class="form-control" placeholder="Title" value="{{ $materiel->nom }}" readonly>
            </div>
            <div class="col mb-3">
                <label class="form-label">Type</label>
                <input type="text" name="price" class="form-control" placeholder="Price" value="{{ $materiel->type }}" readonly>
            </div>
        </div>
        <div class="row">
            <div class="col mb-3">
                <label class="form-label">Marque</label>
                <input type="text" name="product_code" class="form-control" placeholder="Product Code" value="{{ $materiel->marque }}" readonly>
            </div>
            <div class="col mb-3">
                <label class="form-label">Quantité initiale</label>
                <input type="text" class="form-control" name="description" placeholder="Description" readonly value="{{ $materiel->qte }}">
            </div>
        </div>
        <div class="row">
            <div class="col mb-3">
                <label class="form-label">Quantité endomagée</label>
                <input type="text" name="created_at" class="form-control" placeholder="Created At" value="{{ $materiel->qte_end }}" readonly>
            </div>

            <div class="col mb-3">
                <label class="form-label">Etat</label>
                <input type="text" name="price" class="form-control" placeholder="Price" value="{{ $materiel->etat }}" readonly>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label class="form-label">Caractéristique</label>
                <textarea name="created_at" class="form-control" placeholder="Created At" value="" readonly>{{ $materiel->caracteristique }}</textarea>
            </div>
            <div class="col mb-3">
                <div class="form-group">
                    <label for="service_id">Service d'affectation: </label>
                    <select name="service" id="service" class="form-control">
                        <option value="" selected>Sélectionner le service</option>

                        @foreach($services as $key=>$t)
                            <option value='<?=$key?>'>{{$t}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col mb-3">
                <div class="form-group">
                    <label for="id">Technicien: </label>
                    <select name="technicien" id="technicien" class="form-control">
                        <option value="" selected>Sélectionner le Technicien à affecter</option>

                        @foreach($tech as $key=>$t)
                            <option value='<?=$key?>'>{{$t}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>

        <div class="row mt-2" style="text-align: right;">
            <input type="submit" value="valider" class="btn btn-success col-2">
        </div>
    </form>
@endsection
