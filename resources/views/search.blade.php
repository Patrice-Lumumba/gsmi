
    <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Search</title>
    <link href="{{asset('assets/css/bootstrap/bootstrap.min.css')}}" rel="stylesheet">


</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <h3 class="text-center text-danger">Autocomplete search Box</h3>
                <hr>
                <div class="form-group">
                    <h4>Tape ici</h4>
                    <input type="text" class="form-control" name="search" id="search" placeholder="Enter User name">
                </div>
                <div id="search_list"></div>
            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#search').on('keyup',function (){
                var query = $(this).val();
                $.ajax({
                    url: "search",
                    type: "GET",
                    data: {'search': query},
                    success: function (data) {
                        $('#search_list').html(data);
                    }
                });
            });
        });
    </script>


</body>
</html>
