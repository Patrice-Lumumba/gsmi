@php use App\Helpers\Helper;use App\Models\Marque; @endphp
@extends ('layouts.master')

@section('title', 'Marque')
@section('contents')

    <h1 class="">Créer / Ajouter une marque</h1>
    <form action="{{route('marque.store')}}" method="post">
        {{@csrf_field()}}
        <div class="form-group">
            <label for="titre">Titre: </label>
            <input type="text" name="titre" id="titre" placeholder="" class="form-control">
        </div>



        <div class="row mt-2" style="text-align: right;">
            <input type="submit" value="valider" class="btn btn-success col-2">
        </div>
    </form>
@endsection
