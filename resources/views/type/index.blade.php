@php use App\Models\Service;use App\Models\type; @endphp
@extends ('layouts.master')

@section('title', 'Marque')
@section('contents')
    <div class="container">
        <div class="d-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-center">Liste des Ma</h1>
            <a href="{{ route('service.create') }}" class="btn btn-primary" id="toastr-1">Ajouter un service</a>
        </div>
        <hr/>
        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('success') }}
            </div>
        @endif

        @if (Session::has("error"))
            <div class="alert alert-danger alert-dismissable fade show">
                <button class="close" data-dismiss="alert" aria-label="Close">×</button>
                {{session('error')}}
            </div>
            {{--    <p class="alert alert-danger">{{Session::get("error")}}</p>--}}
        @endif

        <table class="table table-bordered">
            <tr>
                <th>#</th>
                <th>Nom du service</th>
                <th>Action</th>
            </tr>
            @if($service->count() > 0)
                @foreach(type::all() as $e)
                    <tr>
                        <td>{{$e->nom}}</td>

                        <td class="align-middle">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="{{ route('type.edit', $e->id)}}" type="button"
                                   class="btn btn-warning">Edit
                                    <i class="fas fa-edit"></i></a>
                                <form action="{{ route('type.destroy', $e->id) }}" method="POST" type="button"
                                      class="btn btn-danger p-0" onsubmit="return confirm('Delete?')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger m-0">Delete <i class="fas fa-trash-alt"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>

                @endforeach
            @else
                <tr>
                    <td class="text-center font-italic" colspan="3">Rien</td>
                </tr>
            @endif
        </table>

        {{--    <button class="btn btn-success"><a href="" va></a></button>--}}
    </div>

@endsection
