@php use App\Helpers\Helper;use App\Models\Materiel; @endphp
@extends ('layouts.master')

@section('title', 'Service')
@section('contents')

    <h1 class="">Créer / Ajouter un service</h1>
    <form action="{{route('service.store')}}" method="post">
        {{@csrf_field()}}
        <div class="form-group">
            <label for="code_mat">Titre: </label>
            <input type="text" name="nom_service" id="nom_service" placeholder="Comptabilité" class="form-control">
        </div>


        <div class="row mt-2" style="text-align: right;">
            <input type="submit" value="valider" class="btn btn-success col-2">
        </div>
    </form>
@endsection
