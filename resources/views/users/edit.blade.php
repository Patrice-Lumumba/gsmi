@extends('layouts.master')

@section('title', 'Edit Users')

@section('contents')
    <h1 class="mb-0">Edit Product</h1>
    <hr />
    <form action="{{ route('users.update', $user->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col mb-3">
                <label class="form-label">Full Name</label>
                <label>
                    <input type="text" name="name" class="form-control" placeholder="Name" value="{{ $user->name }}" >
                </label>
            </div>

            <div class="col mb-3">
                <label class="form-label">Email</label>
                <label>
                    <input type="text" name="email" class="form-control" placeholder="Email" value="{{ $user->email }}" >
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col mb-3">
                <label class="form-label">Phone Number</label>
                <label>
                    <input type="tel" name="tel" class="form-control" value="{{ $user->tel }}" >
                </label>
            </div>
        </div>



        <div class="row">
            <div class="d-grid">
                <button class="btn btn-warning">Update</button>
            </div>
        </div>
    </form>
@endsection
