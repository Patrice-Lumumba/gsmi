@extends('layouts.master')

@section('title', 'List Of Users')

@section('contents')
    <div class="d-flex align-items-center justify-content-between">
        <h1 class="mb-0">Historique des actions</h1>
{{--        <a href="{{ route('users.create') }}" class="btn btn-primary">Add User</a>--}}
    </div>
    <hr />
    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="col-lg-6">
        <div class="form-group">
            <input type="text" class="form-control" name="search" id="search" placeholder="Enter User name">
        </div>
        <div id="search_list"></div>
    </div>
    <div class="col-lg-3"></div>




    <table class="table table-responsive">
        <thead class="table-primary">
        <tr>
            <th>#</th>
            <th>User id</th>
            <th>Action</th>
            <th>Faite le...</th>

            <th>Action</th>
        </tr>
        </thead>
        <tbody>+
        @if($logs->count() > 0)
            @foreach($logs as $log)
                <tr>
                    <td class="align-middle">{{ $loop->iteration }}</td>
                    <td class="align-middle">{{ $log->id }}</td>
                    <td class="align-middle">{{ $log->action }}</td>
                    <td class="align-middle">{{ $log->created_at }}</td>


                </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="5">Nothing to found</td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
