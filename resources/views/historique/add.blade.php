@extends('layouts.master')

@section('title', 'Create User')

@section('contents')

    <?php
        $title = 'Ajouter un matériel'
    ?>
    <title></title>

    <h1 class="mb-0">Add User</h1>
    <hr />
    <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
            <div class="col">
                <input type="email" name="email" class="form-control" placeholder="Email">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="tel" class="form-control" placeholder="Phone Number">
            </div>

        </div>

        <div class="row mb-3">


            <label for="role" class="col-form-label">Status</label>
            <select name="role" class="form-control">
                <option value="">-----Select Role-----</option>
                <option value="admin">Admin</option>
                <option value="user" selected>User</option>
            </select>
        </div>

        <div class="col-md-6">

            @error('role')
            <span class="text-danger">{{$message}}</span>
            @enderror
        </div>

        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
