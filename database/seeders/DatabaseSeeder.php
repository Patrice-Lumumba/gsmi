<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Post;
use App\Models\Technicien;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         \App\Models\User::factory(10)->create();
//         Technicien::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $entries = [

            [
                'name' => 'Admin',
                'tel' => '6884874154',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('123'),
                'role' => 'admin',
                'status' => 'active',

                'created_at' => '2023-07-31',
            ],

            [
                'name' => 'User',
                'tel' => '6859874154',
                'email'=>'user@gmail.com',
                'password' => bcrypt('123'),
                'role' => 'user',
                'status' => 'active',

                'created_at' => '2023-08-31',

            ],
        ];

        $tableName = (new User())->getTable();
        foreach ($entries as $entry) {
            DB::table($tableName)->insert($entry);
        }
    }
}
