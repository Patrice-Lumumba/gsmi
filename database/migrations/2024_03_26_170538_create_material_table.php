<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('materiels', function (Blueprint $table) {
            $table->id();
            $table->string('nom_mat');
            $table->string('type');
            $table->string('qte');
            $table->string('qte_end')->nullable();
            $table->string('marque');
            $table->string('etat');
            $table->timestamp('join_at');
            $table->string('retour');
            $table->string('caracteristique');
//            $table->rememberToken()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('materiels');
    }
};
