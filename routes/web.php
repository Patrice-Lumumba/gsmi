<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\MarqueController;
use App\Http\Controllers\MaterielController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\TechnicienController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/login', [LoginController::class, 'index'])->name("login");
Route::post('/login', [LoginController::class, 'store'])->middleware(['guest']);


Route::get('/register', [RegisterController::class, 'index'])->middleware(['guest'])
    ->name('register');
Route::post('/register', [RegisterController::class, 'store'])->middleware('guest');
Route::get('logout', [LoginController::class, 'logout'])->name('logout');

Route::get('dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::controller(UserController::class)->prefix('users')->group(function () {
    Route::get('', 'index')->name('users');
    Route::get('create', 'create')->name('users.create');
    Route::post('store', 'store')->name('users.store');
    Route::get('show/{id}', 'show')->name('users.show');
    Route::get('edit/{id}', 'edit')->name('users.edit');
    Route::put('edit/{id}', 'update')->name('users.update');
    Route::delete('destroy/{id}', 'destroy')->name('users.destroy');
});

//Route des matériels

Route::controller(MaterielController::class)->prefix('materiel')->group(function () {
    Route::get('', 'index')->name('materiel');
    Route::get('create', 'create')->name('materiel.create');
    Route::post('store', 'store')->name('materiel.store');
    Route::get('show/{id}', 'show')->name('materiel.show');
    Route::get('edit/{id}', 'edit')->name('materiel.edit');
    Route::put('edit/{id}', 'update')->name('materiel.update');
    Route::delete('destroy/{id}', 'destroy')->name('materiel.destroy');
});

//Route des services

Route::controller(ServiceController::class)->prefix('service')->group(function () {
    Route::get('', 'index')->name('service');
    Route::get('create', 'create')->name('service.create');
    Route::post('store', 'store')->name('service.store');
    Route::get('show/{service_id}', 'show')->name('service.show');
    Route::get('edit/{service_id}', 'edit')->name('service.edit');
    Route::put('edit/{service_id}', 'update')->name('service.update');
    Route::delete('destroy/{service_id}', 'destroy')->name('service.destroy');
});

//Route des techniciens

Route::controller(TechnicienController::class)->prefix('technicien')->group(function () {
    Route::get('', 'index')->name('technicien');
    Route::get('create', 'create')->name('technicien.create');
    Route::post('store', 'store')->name('technicien.store');
    Route::get('show/{id}', 'show')->name('technicien.show');
    Route::get('edit/{id}', 'edit')->name('technicien.edit');
    Route::put('edit/{id}', 'update')->name('technicien.update');
    Route::delete('destroy/{id}', 'destroy')->name('technicien.destroy');
});

Route::controller(MarqueController::class)->prefix('marque')->group(function () {
    Route::get('', 'index')->name('marque');
    Route::get('create', 'create')->name('marque.create');
    Route::post('store', 'store')->name('marque.store');
    Route::get('show/{id}', 'show')->name('marque.show');
    Route::get('edit/{id}', 'edit')->name('marque.edit');
    Route::put('edit/{id}', 'update')->name('marque.update');
    Route::delete('destroy/{id}', 'destroy')->name('marque.destroy');
});

Route::controller(MarqueController::class)->prefix('marque')->group(function () {
    Route::get('', 'index')->name('marque');
    Route::get('create', 'create')->name('marque.create');
    Route::post('store', 'store')->name('marque.store');
    Route::get('show/{id}', 'show')->name('marque.show');
    Route::get('edit/{id}', 'edit')->name('marque.edit');
    Route::put('edit/{id}', 'update')->name('marque.update');
    Route::delete('destroy/{id}', 'destroy')->name('marque.destroy');
});

Route::get("search", [PostController::class, 'search']);

Route::controller(LogController::class)->prefix('logs')->group(function () {
    Route::get('', 'index')->name('logs');
});
//Route::get('/', function () {
//    return view('auth.login');
//});
//Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'admin']], function () {
//    Route::get('/', [AdminController::class, 'index'])->name('admin');
//    Route::resource('users', UserController::class);
//    Route::get('/profile', [AdminController::class, 'profile'])->name('admin-profile');
//    Route::post('/profile/{id}', [AdminController::class, 'profileUpdate'])->name('profile-update');
//});
